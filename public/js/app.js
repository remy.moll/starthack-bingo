"use strict";
const container = document.getElementById("bingo-grid");
const grid_items = 16;
const fetchData = (card_data) => {
    let cards = "";
    card_data.forEach(function (value) {
        cards += create_card(value);
    });
    // for (let i = 1; i <= grid_items; i++) {
    //   // let card: ICard = {
    //   //   id: i,
    //   //   title: `Task no ${i}`,
    //   //   image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
    //   //   description: "Please do the task Please do the task Please do the task Please do the task Please do the task Please do the task Please do the task Please do the task Please do the task Please do the task"
    //   // }
    //   let card = card_data[i]
    //   cards += create_card(card)
    // }
    let column_headings = ["I", "II", "III", "IV"].map(x => `<div class="col"><h3 class="text-center">Category ${x}</h3></div>`).join("");
    container.innerHTML = cards + container.innerHTML; // add ${column_headings}?
};
const create_card = (card) => {
    // on a small screen only the image for each challenge is shown. Further information will be displayed on button press
    let modal_html = `
  <div class="modal fade" id="modal-${card.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="position-absolute top-0 end-0">
            <button type="button" class="btn-close p-2" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="row g-0 align-items-center">
            <div class="col-md-4">
              <img src="${card.image_src}" class="img-fluid rounded-start mx-auto d-block" alt="challenge image">
            </div>
            <div class="col-md-8">
                <h2 class="card-title text-center mb-1">${card.title}</h2>
                <p class="card-text lead" text-center>${card.description}</p>

                <hr/>

                <h4 class="card-title text-center mb-1">Proof <span class="badge">${card.points} points</span></h4>
                <p class="card-text lead" text-center>${card.proof}</p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  `;
    // on a large screen show both image + challenge name. The lightbox still exists.
    let card_content_html = `
    <div class="row g-0 align-items-center h-100">
      <div class="col-md-4">
        <img src="${card.image_src}" class="img-fluid mx-auto d-block" alt="challenge image">
      </div>
      <div class="col-md-8 d-none d-sm-inline">
        <div class="card-body">
          <h5 class="card-title text-center">${card.title}</h5>
        </div>
      </div>
    </div>
  `;
    let card_html = `
  <div class="col">
    <a class="card shadow-lg h-100" data-bs-toggle="modal" data-bs-target="#modal-${card.id}" style="color: inherit; text-decoration: none;" href="#">
      
      ${card_content_html}
    </a>
  </div>
  ${modal_html}`;
    return card_html;
};
// =============================================================================//
// Card data
let card_data = [
    {
        id: 1,
        title: `Attend the Pitching Competition Final`,
        image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_1.svg",
        description: "START is devoted to fostering entrepreneurship. As such one of our hallmark events is the Pitching Competition which takes place at the end of the Summit. Here we have a collection of Start-Ups compete for 10,000 CHF in funding by pitching their ideas to a committee of investors. Your task is to attend the Pitching Competition Final.",
        points: 10,
        proof: "Provide a selfie of yourself at the pitching competition final. "
    },
    {
        id: 2,
        image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_2.svg",
        title: `Attend our Pub Crawl`,
        description: "St. Gallen is renowned for its local drinks culture with exceptional beer brands like Schützengarten,This task consists of three steps. First find the steps, second execute the steps, step three profit. a partner of START. To get acquainted with St. Gallen quickly we have organized a Pub Crawl on the first night of the Summit. Join us and get to know what St. Gallen has to offer! Your task is to attend the Summit Pub Crawl. You can sign up in the Event Page on the app.",
        points: 4,
        proof: "Provide a personal selfie at one of the pubs featured in the Pub Crawl. It can be a selfie with a beer or a picture with your group leader."
    },
    {
        id: 3,
        title: `Post about START Summit on LinkedIn`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "Let your friends know you are coming to the START Summit! It is vital for future Summit events and lets your network know the cool events that you are involved in! As an example for a post, you can go to <a href=\"https://start-global.shinyapps.io/START_Summit_Share/\">https://start-global.shinyapps.io/START_Summit_Share/</a> to automatically generate a custom Summit Picture Frame. ",
        points: 4,
        proof: "Provide a Screenshot of your post."
    },
    {
        id: 4,
        title: `Visit Base Camp Stage`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "The Summit is divided into multiple stages; Base Camp Stage, Mountaineers’ Stage, and Summit Stage. To begin your journey, go to the Base Camp Stage. Here you will get to attend Fireside Chats (a two person discussion) and Panels (multiple speakers discussions). START has organized incredible speakers to give you invaluable insights and an unforgettable experience. For a full list of events and speakers the SECTION on the App. Your task is to attend either a Fireside-Chat or a Panel.",
        points: 6,
        proof: "Take a selfie at one of the events!"
    },
    {
        id: 5,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    {
        id: 6,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    {
        id: 7,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    {
        id: 8,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    {
        id: 9,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    {
        id: 10,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    {
        id: 11,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    {
        id: 12,
        title: `Find all X`,
        image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
        description: "This task consists of three steps. First find the steps, second execute the steps, step three profit.",
        points: 5,
        proof: "Just tell us nicely that you did it. We trust you."
    },
    // {
    //   id: 13,
    //   title: `Find all X`,
    //   image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
    //   description: "This task consists of three steps. First find the steps, second execute the steps, step three profit."
    // },
    // {
    //   id: 14,
    //   title: `Find all X`,
    //   image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
    //   description: "This task consists of three steps. First find the steps, second execute the steps, step three profit."
    // },
    // {
    //   id: 15,
    //   title: `Find all X`,
    //   image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
    //   description: "This task consists of three steps. First find the steps, second execute the steps, step three profit."
    // },
    // {
    //   id: 16,
    //   title: `Find all X`,
    //   image_src: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png",
    //   description: "This task consists of three steps. First find the steps, second execute the steps, step three profit."
    // }
];
// and run the code:
fetchData(card_data);
