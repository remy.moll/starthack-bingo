const container: HTMLElement | any = document.getElementById("bingo-grid")
const grid_items: number = 16


interface ICard {
  id: number;
  title: string;
  image_src: string;
  description: string;
  points: number;
  proof: string;
}

const fetchData = (card_data: ICard[]): void => {
  let cards: string = ""
  
  card_data.forEach(function(value){
    cards += create_card(value)
  })

  let column_headings: String = ["I", "II", "III"].map(x => `<div class="col"><h3 class="text-center">Category ${x}</h3></div>`).join("")
  container.innerHTML = cards  // add ${column_headings}?
}
  

const create_card = (card: ICard): string => {
  let modal_html: string = `
  <div class="modal" id="modal-${card.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="position-absolute top-0 end-0">
            <button type="button" class="btn-close p-2 btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="row g-0 align-items-center">
            <div class="col-md-4 text-center">
            
              <img src="${card.image_src}" class="img-fluid rounded-start mx-auto d-block header_img" alt="challenge image">
              <h2><span class="badge mt-2 mx-auto">${card.points} points</span></h2>
            </div>
            <div class="col-md-8">
                <h3 class="text-center mb-1 h-upper">${card.title}</h3>
                <p class="card-text lead" text-center>${card.description}</p>

                <hr/>

                <h3 class="text-center mb-1 h-upper">Proof</h3>
                <p class="card-text lead" text-center>${card.proof}</p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  `
  // on a small screen only the image for each challenge is shown. Further information will be displayed on button press
  // on a large screen show both image + challenge name. The lightbox still exists.
  let card_content_html: string = `
    <div class="row g-0 align-items-center h-100">
      <div class="col-md-4 p-2">
        <div class="ratio ratio-1x1">
          <img src="${card.image_src}" class="img-fluid mx-auto d-block" alt="challenge image">
        </div>
      </div>
      <div class="col-md-8 d-none d-md-inline">
        <div class="card-body">
          <h5 class="card-title text-center">${card.title}</h5>
        </div>
      </div>
    </div>
  `

  let card_html: string = `
  <div class="col">
    <a class="card shadow-lg h-100" data-bs-toggle="modal" data-bs-target="#modal-${card.id}" style="color: inherit; text-decoration: none;" href="#">
      
      ${card_content_html}
    </a>
  </div>
  ${modal_html}`
  return card_html
}
  
  

// =============================================================================//
// Card data

let card_data: ICard[] = [
  {
    id: 1,
    title: `Attend the Pitching Competition Final`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_pitching_competition.svg",
    description: "START is devoted to fostering entrepreneurship. As such one of our hallmark events is the Pitching Competition which takes place at the end of the Summit. Here we have a collection of Start-Ups compete for 10,000 CHF in funding by pitching their ideas to a committee of investors. Your task is to attend the Pitching Competition Final.",
    points: 10,
    proof: "Provide a selfie of yourself at the pitching competition final. "
  },
  {
    id: 9,
    title: `Connect with one of the START Fellows`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_start_fellowship.svg",
    description: "START Fellowship lies at the heart of START as one of its four fundamental missions. Every year, 40 entrepreneurs from across Latin America (Colombia, Mexico, Ecuador, Brazil, and Peru) cross the Atlantic from the Andes Mountains to the Swiss Alps. Here they spend 6 months at the University of St. Gallen and START devoted to taking their venture to the next level. Our START Fellows will be spread out throughout the START Summit gaining exposure to investors and validating their start-ups. Your task is to find one of them and connect with them. Get to know their company and network with them.",
    points: 10,
    proof: "Take a selfie in one of the Fellowship booths or a selfie with one of the fellows. You can also take a picture of the badge of one of the fellows you connected with."
  },
  {
    id: 5,
    title: `Visit the Sustainability Lounge`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_sustainability_lounge.svg",
    description: "“I <it>am</it> somebody. I can be part of the change”. At the Sustainability Lounge, you will discover many ways to connect with other people passionate about addressing environmental and social issues while fostering financial opportunities. Go there and get inspired!",
    points: 10,
    proof: "Take a selfie at the Sustainability Lounge swing and send it to us!"
  },
  {
    id: 10,
    title: `Visit START Hack`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_start_hack.svg",
    description: "START Hack is one of START's four core missions. Every year we bring together and showcase the up-and-coming tech talent from across Europe. By sourcing cutting-edge challenges from leading Tech companies, we offer participants the chance to compete to solve these challenges and demonstrate their capabilities. With 30,000 CHF in prices and networking opportunities on the line, there are many reasons to participate. We encourage you to pass by and take a look to see what cool projects everyone is working on! START Hack is hosted in the Hall next to START Summit. Your task is to visit the START Hack.",
    points: 8,
    proof: "Take a selfie inside the START Hack hall and send it to us!"
  },
  {
    id: 6,
    title: `Visit the START Fair`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_start_fair.svg",
    description: "START Fair showcases all START initiatives. You will find the hottest new prototypes, the sickest booths, and custom meeting spaces, allowing you to truly delve into what the entrepreneurial ecosystem is all about. Engage the “booth concept” and try out the cool products from the Startups. Your task is to attend the START Fair!",
    points: 10,
    proof: "Take a photo of you engaging the booth concept and send it to us!"
  },
  {
    id: 11,
    title: `Fill Summit Feedback Form`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_feedback_form.svg",
    description: "Help us to improve the experience at the Start Summit and fill out the Summit Feedback Form. The Feedback forms are very important to us and will make a huge impact to the next START summit! Let's make the next START Summit even cooler!",
    points: 8,
    proof: "Take a screenshot of the verification page that follows after the form has been submitted."
  },
  {
    id: 7,
    title: `Connect with one Student`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_students.svg",
    description: "The purpose of the START Summit is to bring people together. Amongst the participants you will find the future Bill Gates, Nicolas Hayek, and Oprahs of the world. That is why we encourage you to network with them. This way you may discover the future leaders of enterprise in others and in yourself! Your task is to connect with another student.",
    points: 6,
    proof: "Take a selfie with another student or take a picture or their badge."
  },
  {
    id: 8,
    title: `Connect with one Corporate Partner`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_corporate_partners.svg",
    description: "Corporate partners provide the backbone of the START Summit. We are very grateful for their supports. All of them will be represented at the Summit with their own booths where they will showcase their latest technologies.",
    points: 6,
    proof: "Take a selfie at a Corporate Partner Booth."
  },
  {
    id: 4,
    title: `Visit Base Camp Stage`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_base_camp.svg",
    description: "The Summit is divided into multiple stages; Base Camp Stage, Mountaineers' Stage, and Summit Stage. To begin your journey, go to the Base Camp Stage. Here you will get to attend Fireside Chats (a two person discussion) and Panels (multiple speakers discussions). START has organized incredible speakers to give you invaluable insights and an unforgettable experience. For a full list of events and speakers the SECTION on the App. Your task is to attend either a Fireside-Chat or a Panel.",
    points: 6,
    proof: "Take a selfie at one of the events!"
  },
  {
    id: 12,
    title: `Tell us more about your experience!`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_reflection.svg",
    description: "Reflection is an important aspect of any experience. We would like to reflect on your experience at the START Summit. We won't read them, promise. But we would encourage your to reflect so that your experiences and lessons from the past two days will stay with you longer as you embark on your next expedition!",
    points: 4,
    proof: "Send us a screenshot of your writing."
  },
  {
    id: 3,
    title: `Post about START Summit on LinkedIn`,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_social_media.svg",
    description: "Let your friends know you are coming to the START Summit! It is vital for future Summit events and lets your network know the cool events that you are involved in! As an example for a post, you can go to <a href=\"https://start-global.shinyapps.io/START_Summit_Share/\" target=\"_blank\">https://start-global.shinyapps.io/START_Summit_Share/</a> to automatically generate a custom Summit Picture Frame. ",
    points: 4,
    proof: "Provide a Screenshot of your post."
  },
  {
    id: 2,
    image_src: "https://gitlab.com/remy.moll/starthack-bingo/-/raw/main/public/img/task_pub_crawl.svg",
    title: `Attend our Pub Crawl`,
    description: "St. Gallen is renowned for its local drinks culture with exceptional beer brands like Schützengarten,This task consists of three steps. First find the steps, second execute the steps, step three profit. a partner of START. To get acquainted with St. Gallen quickly we have organized a Pub Crawl on the first night of the Summit. Join us and get to know what St. Gallen has to offer! Your task is to attend the Summit Pub Crawl. You can sign up in the Event Page on the app.",
    points: 4,
    proof: "Provide a personal selfie at one of the pubs featured in the Pub Crawl. It can be a selfie with a beer or a picture with your group leader."
  },
]



// and run the code:
fetchData(card_data)
