# StartHack Bingo

Written in TypeScript because I'm a sucker for types. Bundled with parcel to create a single file that will be served by B2Match.

## Development process
Edit `src/index.html` and `src/app.ts`. Serve with `parcel src/index` which will spin up a local development server.

For production use `parcel build src/index.html`. The file is configurated as to generate a single output file which is already minified. Simply copy/paste `dist/index.html` to B2Match. Voilà!
